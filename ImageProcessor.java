
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;


public class ImageProcessor {
	
	public static void main(String[] args){
		
		/*Declare input output files and readers and writers*/
		String inFile = "urls.txt";
		String outFile = "result.csv";
		
		FileReader fr = null;
		BufferedReader br = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		String delimiter = ";";
		String currentLine = null;
		HashMap<String,String> cache = null;
		ImageProcessor processor = null;
		
		/*Initiate IO objects and process images*/
		try {

			fr = new FileReader(inFile);
			br = new BufferedReader(fr);
			fw = new FileWriter(outFile);
	        bw = new BufferedWriter(fw);
	        cache = new HashMap<String,String>(); //maintain a cache of processed images
	        processor = new ImageProcessor();
	        List<Entry<Integer, Integer>> list = null;
	        
			while((currentLine = br.readLine()) != null){
				
				/*First refer cache to see if image is repeated*/
				if(cache.containsKey(currentLine)){
					bw.write(currentLine);
					bw.write(cache.get(currentLine));
					bw.newLine();
					continue;
				}
				/*process the image to get top 3 prevalent colors*/
				try{
					list = processor.processImage(currentLine,3);
				}catch(IOException  e){
					e.printStackTrace();
					continue;
				}
				
				/*Write the result in csv file and in the cache*/
				if(!list.isEmpty()){
					
					bw.write(currentLine);
					
					Iterator<Map.Entry<Integer, Integer>> it = list.iterator();
					StringBuilder cacheVal = new StringBuilder();
					
					while(it.hasNext()){
			            Map.Entry<Integer, Integer> entry = it.next();
			            bw.write(delimiter);
			            cacheVal.append(delimiter);
			            bw.write(Integer.toHexString(entry.getKey()));
			            cacheVal.append(Integer.toHexString(entry.getKey()));
			        }
					
					cache.put(currentLine, cacheVal.toString());
					bw.newLine();
				}
				
			}

		}catch(IOException e){
			
			e.printStackTrace();
			
		}finally{

			try{

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
				
				if (bw != null)
					bw.close();
				
				if (fw != null)
					fw.close();

			}catch(IOException ex){

				ex.printStackTrace();

			}
		
		}
		
	}
	
	/* opens the provided image url and traverses each pixel to pick its
	 * color. Keeps a count of pixels for each color in a hashmap, sorts the
	 * hashMap and returns a list of required number of prevalent colors
	 *
	 * @param urlString an absolute url of an image
	 * @param max 	maximum number of prevalent colors to be returned
	 * @return 		a list of max prevalent colors and the count of pixels
	 * 
	 */
	private List<Entry<Integer, Integer>> processImage(String urlString, int max) 
							throws MalformedURLException,IOException{
		
		URL url = null;
		BufferedImage image = null;
		int height = 0;
		int width = 0;
		int color = 0;
		int oldVal = 0;
		List<Entry<Integer, Integer>> sortedList = null;
		HashMap<Integer,Integer> colorCountMap;
		
		
		url = new URL(urlString);
		image = ImageIO.read(url);  
		height = image.getHeight();
		width = image.getWidth();
		
		colorCountMap = new HashMap<Integer,Integer>();
		
		for(int i = 0; i < height; i++ ){
			for(int j = 0; j < width; j++){

				color = (image.getRGB(j, i))&0xffffff;//discard alpha value
				if(colorCountMap.containsKey(color)){
					oldVal = colorCountMap.get(color);
					colorCountMap.put(color, oldVal+1);
				}
				else
					colorCountMap.put(color, 1);
					
			}
		}
		
		sortedList = sortMap(colorCountMap);
		colorCountMap.clear();
		if(sortedList.size()>max)
			sortedList.subList(max, sortedList.size()).clear();
            
        return sortedList;
	}
	
	
	/* Sorts the given HashMap by Value
	 *
	 * @param colorCountMap Map containing color as key and pixel count
	 *        as value
	 * @return 		a sorted list of entries from the given HashMap
	 * 
	 */
	private  List<Entry<Integer, Integer>> sortMap(HashMap<Integer,Integer> colorCountMap){
		
		List<Map.Entry<Integer, Integer>> resultList;
		resultList = new LinkedList<Map.Entry<Integer, Integer>>
									(colorCountMap.entrySet());
        Collections.sort(resultList,
                   new Comparator<Map.Entry<Integer, Integer>>() {
                        public int compare(Map.Entry<Integer, Integer> record1,
                                            Map.Entry<Integer, Integer> record2) {
                          if(record2.getValue().equals(record1.getValue())) {
                        	  Integer d1 = new Integer(record1.getKey());
                        	  Integer d2 = new Integer(record2.getKey());
                              return d2.compareTo(d1);
                          }
                          return (record2.getValue()).compareTo(record1.getValue());
                       }
               });
        return resultList;
        
	}
	
}
